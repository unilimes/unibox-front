import React from 'react';
import {render} from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import './index.css';
import rootReducer from './js/reducers/index'
import App from './App';

const store = createStore(rootReducer);

render(
	<Provider store={store}><Router><Route path="/" component={App}/></Router></Provider>,
	document.getElementById('root'));
