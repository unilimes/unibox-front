import React, {Component} from 'react';
import { Switch, Route } from 'react-router'
import { Router } from 'react-router-dom';

import Menu from './js/components/menu';
import Footer from './js/components/footer';
import Home from './js/components/home';
import UsersList from './js/components/users-list';
import Monday from './js/components/monday';

//import App from './js/components/authApp';
import Callback from './js/components/callback';
import Auth from './js/auth0/auth';
import './App.css';
import Grid from "@material-ui/core/Grid";
import {Paper} from "@material-ui/core/Paper";
import withStyles from "@material-ui/core/es/styles/withStyles";


const auth = new Auth();

const handleAuthentication = (nextState, replace) => {
	if (/access_token|id_token|error/.test(nextState.location.hash)) {
		auth.handleAuthentication();
	}
}

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
});


class App extends Component {

	render() {

        const { classes } = this.props;

		return (
			<div style={{ height: '100%', width: '100%'}} className="App">
                <Grid container spacing={24}>
                    <Grid item xs={6} style={{height: '100%'}} sm={3}>

                        <Menu auth={auth}/>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                        <Switch>
                            <Route path="/home" component={Home}/>
                            <Route path="/users" component={UsersList}/>
                            <Route path="/monday" component={Monday}/>
                        </Switch>
                    </Grid>
				</Grid>
				<div>

				</div>
				<Footer/>
			</div>
		);
	}
}

export default withStyles(styles)(App);




// {/*<div>*/}
// 	{/*<Route path="/" render={(props) => <App auth={auth} {...props} />} />*/}
// 	{/*<Route path="/home" render={(props) => <Home auth={auth} {...props} />} />*/}
// 	{/*<Route path="/callback" render={(props) => {*/}
// 		{/*handleAuthentication(props);*/}
// 		{/*return <Callback {...props} />*/}
// 	{/*}}/>*/}
// {/*</div>*/}