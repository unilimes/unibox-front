import {SET_USERINFO} from '../actions/types';
const userInfo = (state = {}, action) => {
	switch (action.type) {
		case SET_USERINFO: {
			state = {...action.payload};
			return {...state};
		}

		default: {
			return {...state};
		}
	}
};
export default userInfo;