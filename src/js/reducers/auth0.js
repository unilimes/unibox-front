import {SET_AUTHCONF} from '../actions/types';
const authData = (state = {}, action) => {
	switch (action.type) {
		case SET_AUTHCONF: {
			state = {...action.payload};
			return {...state};
		}

		default: {
			return {...state};
		}
	}
};
export default authData;