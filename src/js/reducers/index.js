import { combineReducers } from 'redux';
import userList from './userList'
import userInfo from './userInfo'
import authData from './auth0';

export default combineReducers({
	userList,
	userInfo,
	authData
})