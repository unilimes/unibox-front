import {SET_USERLIST} from '../actions/types'
const userList = (state = {}, action) => {
	switch (action.type) {
		case SET_USERLIST: {
			state.users = action.payload;
			return {...state};
		}
		default: {
			return {...state};
		}
	}
};
export default userList;