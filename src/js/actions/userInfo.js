import {SET_USERINFO} from './types'

export const setUserInfo = info => ({
	type: SET_USERINFO,
	payload: info
});