import {SET_USERLIST} from './types'
export const setUsersArray = users => ({
	type: SET_USERLIST,
	payload: users
})