import {SET_AUTHCONF} from './types'

export const setAuthConf = data => ({
	type: SET_AUTHCONF,
	payload: data
});