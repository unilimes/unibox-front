import React, {Component} from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles'

const styles = theme => ({
    root: {},
});

class Monday extends Component {

    componentDidMount() {
    }

    render() {
        const {classes} = this.props;

        return (
            <div className={classes.root}>
                <iframe src="https://www.bitrix24.ua/" width="540" height="450"></iframe>
            </div>
        );
    }
}

export default compose(
    connect(),
    withStyles(styles)
)(Monday);
