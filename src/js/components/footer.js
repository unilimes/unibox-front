import React, {Component} from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';


const muiStyles = theme => ({
	container: {
		height: '70px',
		backgroundColor: 'black',
		position: 'absolute',
		bottom: 0
	},
	item: {
		marginTop: '25px',
		color: 'white'
	}
});

class Footer extends Component {
	render() {
		const {classes} = this.props;
		return (
			<Grid container classes={{container: classes.container}}>
				<Grid item xs={12} classes={{item: classes.item}}>
					<Typography color="inherit" variant="title">
						Footer
					</Typography>
				</Grid>
			</Grid>
		);
	}
}

export default compose(
	connect(
		(state) => ({}),
		() => ({})
	),
	withStyles(muiStyles)
)(Footer);
