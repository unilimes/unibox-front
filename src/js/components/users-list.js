import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import axios from "axios/index";
import {setUsersArray} from "../actions/userList";
import {compose} from "redux";
import {connect} from "react-redux";

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
});

let id = 0;
function createData(name, calories, fat, carbs, protein) {
    id += 1;
    return { id, name, calories, fat, carbs, protein };
}

const data = [
    createData('Rus', 159, 6.0, 24, 4.0),
    createData('Ed', 237, 9.0, 37, 4.3),
    createData('Max', 262, 16.0, 24, 6.0),
];

class UsersList extends React.Component{
    componentDidMount(){
        axios.get('http://localhost:7000/api/users').then((res) => {
            this.props.setUsersArray(res.data);
        }).catch((err) => {
            console.error(err);
        });
    }

    render() {
        const { classes } = this.props;
        return (
            this.props.users ? (<Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell numeric>Name</TableCell>
                            <TableCell numeric>Birthday</TableCell>
                            <TableCell numeric>Email</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.users.map(user => {
                            return (
                                <TableRow key={user.user_id}>
                                    <TableCell numeric>{user.user_metadata.name}</TableCell>
                                    <TableCell numeric>{user.user_metadata.byrthday}</TableCell>
                                    <TableCell numeric>{user.user_metadata.email}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>) : (<CircularProgress className={classes.progress} size={50} />)
        )
    }
}
export default compose(
    connect(
        (state) => ({
            users: state.userList.users,
            userInfo: state.userInfo
        }),
        (dispatch) => ({
            setUsersArray: usersArray => dispatch(setUsersArray(usersArray))
        })
    ),
    withStyles(styles)
)(UsersList);
