import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';


const muiStyles = theme => ({
	container: {

	},
	item: {
		position: 'absolute',
		top: '50%',
		transform: 'translate(-50%,-50%)',
		left: '50%'
	}
});

class Home extends Component {
	render() {
		const {classes} = this.props;
		return (
			<Grid container classes={{container: classes.container}}>
				<Grid item xs={12} classes={{item: classes.item}}>
					<Typography color="inherit" variant="display4">
						Welcome to HomePage!
					</Typography>
				</Grid>
			</Grid>
		);
	}
}


export default withStyles(muiStyles)(Home);
