import React, {Component} from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles'
import {Link} from 'react-router-dom'
// import AppBar from '@material-ui/core/AppBar';
// import Tabs from '@material-ui/core/Tabs';
// import Tab from '@material-ui/core/Tab';
// import Button from '@material-ui/core/Button';

import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';


import {setAuthConf} from '../actions/auth0';

const muiStyles = theme => ({
	root: {
		backgroundColor: 'red'
	}
});

const style = theme => ({
	// link: {
	// 	textDecoration: 'none',
	// 	color: '#ffffff'
	// }
    // menuList: {
     //    width: '200px',
	// 	height: '100%'
	// },
    menuItem: {
        '&:focus': {
            backgroundColor: theme.palette.primary.main,
            '& $primary, & $icon': {
                color: theme.palette.common.white,
            },
        },
    },
    primary: {},
    icon: {},
});

class Menu extends Component {

	componentDidMount() {

		console.log(this.props.auth.isAuthenticated());
		this.props.setAuthConf({'auth': this.props.auth.isAuthenticated()});

	}


	render() {
		// const bool = true;
		const {classes} = this.props;
		return (
            <Paper className={classes.menuList}>
                <MenuList >
                    <MenuItem className={classes.menuItem}>
                        <ListItemIcon className={classes.icon}>
                            <SendIcon />
                        </ListItemIcon>
                        <ListItemText classes={{ primary: classes.primary }} inset primary={<Link style={style.link} to="users">People</Link>} />
                    </MenuItem>
                    <MenuItem className={classes.menuItem} to="users">
                        <ListItemIcon className={classes.icon}>
                            <DraftsIcon />
                        </ListItemIcon>
                        <ListItemText classes={{ primary: classes.primary }} inset primary={<Link style={style.link} to="monday">Monday</Link>} />
                    </MenuItem>
                    <MenuItem className={classes.menuItem}>
                        <ListItemIcon className={classes.icon}>
                            <InboxIcon />
                        </ListItemIcon>
                        <ListItemText classes={{ primary: classes.primary }} inset primary="Inbox" />
                    </MenuItem>
                </MenuList>
            </Paper>

			// {/*<AppBar classes={{root: classes.root} } position='static'>*/}
			// 	{/*<Tabs classes={{root: classes.root}} value={false}>*/}
			// 		{/*<Tab*/}
			// 			{/*label={<Link style={style.link} to="home">Home</Link>}*/}
			// 		{/*/>*/}
			// 		{/*<Tab*/}
			// 			{/*label={<Link style={style.link} to="users">User List</Link>}*/}
			// 		{/*/>*/}
			// 		{/*<Tab onClick={() => {*/}
            //
			// 			{/*this.props.authData.auth ? this.props.auth.logout() : this.props.auth.login()*/}
            //
			// 			{/*this.props.setAuthConf({'auth': this.props.auth.isAuthenticated()});*/}
            //
			// 		{/*}} style={{right: 0, position: 'absolute'}}*/}
			// 			 {/*label={ this.props.authData.auth  ? "Log Out" : "Log In"}*/}
			// 		{/*/>*/}
			// 	{/*</Tabs>*/}
			// {/*</AppBar>*/}

		);
	}
}

export default compose(
	connect(
		(state) => ({
			authData: state.authData
		}),
		(dispatch) => ({
			setAuthConf: authData => dispatch(setAuthConf(authData))
		})
	),
	withStyles(style)
)(Menu);

// export default withStyles(styles)(MenuListComposition);