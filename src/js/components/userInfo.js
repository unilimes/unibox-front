import React, {Component} from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
	container: {
		paddingTop: '30px'
	},

});

class userInfo extends Component {
	render() {
		console.log('INFO', this.props);
		const {classes} = this.props;
		return (
			<Grid container classes={{container: classes.container}}>
				<Grid item xs={12}>
					<Typography color="inherit" variant="title">
						Jira ID:
						{
							this.props.userInfo.user.jira_id
						}
					</Typography>
				</Grid>
				<Grid item xs={12}>
					<Typography color="inherit" variant="title">
						Name:
						{
							this.props.userInfo.user.name
						}
					</Typography>
				</Grid>
				<Grid item xs={12}>
					<Typography color="inherit" variant="title">
						Yaware Id:
						{
							this.props.userInfo.user.yaware_id
						}
					</Typography>
				</Grid>
			</Grid>
		);
	}
}

export default compose(
	connect(
		(state) => ({
			userInfo: state.userInfo
		}),
		() => ({})
	),
	withStyles(styles)
)(userInfo);
