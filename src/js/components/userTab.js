import React, {Component} from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles'
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import axios from 'axios';

import {setUserInfo} from '../actions/userInfo';

const styles = theme => ({
	title: {
		color: 'white',
	},
	titleBar: {
		background: 'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
	},
});


class userTab extends Component {
	getInfo = () => {
		console.log(this.props);

		let send_params = {
			dateFrom: '2018-05-24',
			dateTo: '2018-05-24',
			user: this.props
		};

		// axios({
		// 	method: 'post',
		// 	url: 'http://192.168.1.101:7000/api/user',
		// 	data: {
		// 		firstName: 'Fred',
		// 		lastName: 'Flintstone'
		// 	},
		// 	headers:{
		// 		authorization: `Bearer ${this.getAccessToken()}`,
		// 		'Content-type':	'application/x-www-form-urlencoded'
		// 	}
		// }).then((res) => {
		//
		// 	console.log(res);
		//
		// 	this.props.setUserInfo(res)
		//}); 'Content-type': 'application/x-www-form-urlencoded'
		// 'Content-type': 'application/x-www-form-urlencoded'
		axios.post('http://192.168.1.101:7000/api/user',  send_params, {headers: { authorization: `Bearer ${this.getAccessToken()}`,}}).then((res) => {

			console.log('RES',res);

			this.props.setUserInfo(res.data)
		});


	};


	getAccessToken() {
		const accessToken = localStorage.getItem('access_token');
		if (!accessToken) {
			throw new Error('No access token found');
		}
		return accessToken;
	}


	render() {
		const {classes} = this.props;
		return (
			<GridListTile onClick={this.getInfo} key={this.props.img}>
				<img src={this.props.avatar} alt={this.props.name}/>
				<GridListTileBar
					title={this.props.name}
					classes={{
						root: classes.titleBar,
						title: classes.title,
					}}
				/>
			</GridListTile>
		);
	}
}

export default compose(
	connect(
		() => ({}),
		(dispatch) => ({
			setUserInfo: userInfo => dispatch(setUserInfo(userInfo))
		})
	),
	withStyles(styles)
)(userTab);
