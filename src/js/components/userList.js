import React, {Component} from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles'
import UserInfo from './userInfo';
import GridList from '@material-ui/core/GridList';
import axios from 'axios';
import Auth from '../auth0/auth';

import {setUsersArray} from '../actions/userList';

import UserTab from  './userTab';

const styles = theme => ({
	root: {
		display: 'flex',
		flexWrap: 'wrap',
		justifyContent: 'space-around',
		overflow: 'hidden',
		backgroundColor: theme.palette.background.paper,
	},
	gridList: {
		flexWrap: 'nowrap',
		// Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
		transform: 'translateZ(0)',
	},
	title: {
		color: 'white',
	},
	titleBar: {
		background: 'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
	},
});

class userList extends Component {

	componentDidMount() {

		axios.get('http://192.168.1.101:7000/api/users', {headers: { authorization: `Bearer ${this.getAccessToken()}` }}).then((res) => {
			this.props.setUsersArray(res.data);

		}).catch((err) => {
			console.log(err, 'catch eror');
		});
	}

	getAccessToken() {
		const accessToken = localStorage.getItem('access_token');
		if (!accessToken) {
			throw new Error('No access token found');
		}
		return accessToken;
	}

	render() {

		//const { isAuthenticated } = this.props.auth;
		const {classes} = this.props;

		return (
			<div className={classes.root}>
				{

					this.props.users && this.props.users.length ?
						<GridList className={classes.gridList} cols={2.5}>
							{
								this.props.users.map((user, key) => (
									<UserTab key={key} avatar={user.avatar} name={user.name} jira_id={user.jira_id} id={user.id} yaware_id={user.yaware_id}/>
								))
							}
						</GridList>
						: null
				}
				{

					this.props.userInfo.trackedDiff ? <UserInfo/> : null
				}
			</div>
		);
	}
}

export default compose(
	connect(
		(state) => ({
			users: state.userList.users,
			userInfo: state.userInfo
		}),
		(dispatch) => ({
			setUsersArray: usersArray => dispatch(setUsersArray(usersArray))
		})
	),
	withStyles(styles)
)(userList);
